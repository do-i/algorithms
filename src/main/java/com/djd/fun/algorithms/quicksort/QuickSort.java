package com.djd.fun.algorithms.quicksort;

import com.google.common.annotations.VisibleForTesting;

/**
 * Quick Sort
 *
 * <p>Step 1: use the last value in the given array as pivot value. Step 2: partition the array into
 * left and right of pivot location Step 3: recursively perform Step 1 and 2.
 */
public class QuickSort {

  public static void sort(int[] data) {
    if (data.length < 2) {
      return;
    }
    quickSort(data, 0, data.length - 1);
  }

  private static void quickSort(int[] data, int startIdx, int endIdx) {
    int partitionIdx = partition(data, startIdx, endIdx);
    // handle right side of pivot
    if (partitionIdx - 1 > startIdx) {
      quickSort(data, startIdx, partitionIdx - 1);
    }
    // handle left side of pivot
    if (partitionIdx + 1 < endIdx) {
      quickSort(data, partitionIdx + 1, endIdx);
    }
  }

  /** @return position of the pivot location */
  @VisibleForTesting
  static int partition(int[] data, int startIdx, int endIdx) {
    // always use the end index but it can be random index between start and end index
    int pivot = data[endIdx];
    int partitionIdx = startIdx;
    for (int i = startIdx; i < endIdx; i++) {
      if (data[i] < pivot) {
        swap(data, i, partitionIdx);
        partitionIdx++;
      }
    }
    // move pivot value to pivot location
    swap(data, partitionIdx, endIdx);
    return partitionIdx;
  }

  @VisibleForTesting
  static void swap(int[] data, int fromIdx, int toIdx) {
    if (fromIdx < 0 || toIdx < 0) {
      throw new IllegalArgumentException("fromIdx and toIdx should be > 0");
    }
    if (fromIdx >= data.length || toIdx >= data.length) {
      throw new IllegalArgumentException("fromIdx and toIdx should be < " + data.length);
    }
    if (fromIdx == toIdx) {
      return;
    }
    int tmp = data[fromIdx];
    data[fromIdx] = data[toIdx];
    data[toIdx] = tmp;
  }
}
