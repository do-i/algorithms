package com.djd.fun.algorithms.binpacking;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;
import java.util.Collection;
import java.util.Comparator;
import java.util.StringJoiner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** A implementation of Bin Packing Problem with First Fit Decreasing algorithm. */
public class FirstFitDecreasing {

  private static final Logger log = LoggerFactory.getLogger(FirstFitDecreasing.class);
  private final ImmutableList<BaseItem> items;
  private final long binCapacity;

  /**
   * @param items
   * @param binCapacity should be larger than the max weight of items
   */
  public FirstFitDecreasing(Collection<BaseItem> items, long binCapacity) {
    this.items =
        items
            .stream()
            .sorted(Comparator.comparingLong(BaseItem::getWeight).reversed())
            .collect(ImmutableList.toImmutableList());
    this.binCapacity = checkCapacity(items, binCapacity);
  }

  /** @return number of bins required */
  public int solve() {
    Multimap<Integer, BaseItem> itemsByIndex = HashMultimap.create();
    int numOfBins = 0;
    long[] remainSpace = new long[items.size()];

    for (int itemIndex = 0; itemIndex < items.size(); itemIndex++) {
      BaseItem item = items.get(itemIndex);
      long weight = item.getWeight();
      int remainSpaceIndex = 0;
      for (; remainSpaceIndex < numOfBins; remainSpaceIndex++) {
        if (remainSpace[remainSpaceIndex] >= weight) {
          remainSpace[remainSpaceIndex] -= weight;
          itemsByIndex.put(remainSpaceIndex, item);
          break;
        }
      }

      if (remainSpaceIndex == numOfBins) {
        remainSpace[remainSpaceIndex] = binCapacity - weight;
        itemsByIndex.put(remainSpaceIndex, item);
        numOfBins++;
      }
    }
    log.info("itemsByIndex: {}", itemsByIndex);
    return numOfBins;
  }

  private static long checkCapacity(Collection<BaseItem> items, long binCapacity) {
    if (binCapacity
        < items
            .stream()
            .max(Comparator.comparingLong(BaseItem::getWeight))
            .map(BaseItem::getWeight)
            .get()) {
      throw new IllegalArgumentException(
          "binCapacity is requierd to be at least as big as max weight");
    }
    return binCapacity;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", FirstFitDecreasing.class.getSimpleName() + "[", "]")
        .add("items=" + items)
        .add("binCapacity=" + binCapacity)
        .toString();
  }
}
