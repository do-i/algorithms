package com.djd.fun.algorithms.binpacking;

import com.djd.fun.io.MoreResources;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SampleCase {

  private static final Logger log = LoggerFactory.getLogger(SampleCase.class);

  public static void main(String[] args) throws IOException {
    CsvSchema schema = CsvSchema.emptySchema().withHeader();
    MappingIterator<BaseItem> mappingIterator =
        new CsvMapper()
            .readerFor(MediaFileItem.class)
            .with(schema)
            .readValues(MoreResources.getResource("mediaList.csv"));
    FirstFitDecreasing firstFitDecreasing = new FirstFitDecreasing(mappingIterator.readAll(), 4400);
    log.info("Number of bins required : " + firstFitDecreasing.solve());
  }
}
