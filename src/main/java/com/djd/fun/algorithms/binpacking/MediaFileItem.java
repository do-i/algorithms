package com.djd.fun.algorithms.binpacking;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MediaFileItem extends BaseItem {

  public MediaFileItem(@JsonProperty("name") String fileName, @JsonProperty("size") long fileSize) {
    super(fileName, fileSize);
  }

  public static MediaFileItem from(String fileName, long fileSize) {
    return new MediaFileItem(fileName, fileSize);
  }
}
