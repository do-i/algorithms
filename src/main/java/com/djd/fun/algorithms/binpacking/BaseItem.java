package com.djd.fun.algorithms.binpacking;

import static com.djd.fun.base.MorePredicate.checkGreaterThanZero;
import static com.djd.fun.base.MorePredicate.checkNotBlank;

import java.util.Objects;
import java.util.StringJoiner;

public class BaseItem {

  private final String name;
  private final long weight;

  public BaseItem(String name, long weight) {
    this.name = checkNotBlank(name);
    this.weight = checkGreaterThanZero(weight);
  }

  public String getName() {
    return name;
  }

  public long getWeight() {
    return weight;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    BaseItem baseItem = (BaseItem) o;
    return weight == baseItem.weight && name.equals(baseItem.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, weight);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", BaseItem.class.getSimpleName() + "[", "]")
        .add("name='" + name + "'")
        .add("weight=" + weight)
        .toString();
  }
}
