package com.djd.fun.base;

import javax.annotation.Nullable;

public class MorePredicate {

  /**
   * @param value to be tested for {@code null} or blank
   * @return value
   * @throws IllegalArgumentException if value is either {@code null} or blank
   */
  public static String checkNotBlank(@Nullable String value) {
    if (value == null || value.trim().isEmpty()) {
      throw new IllegalArgumentException("Invalid value: " + value);
    }
    return value;
  }

  /**
   * @param value to be tested for > 0
   * @return value
   * @throws IllegalArgumentException if value is not greater than zero
   */
  public static int checkGreaterThanZero(int value) {
    if (value <= 0) {
      throw new IllegalArgumentException(
          String.format("value (%d) is not greater than zero", value));
    }
    return value;
  }

  /**
   * @param value to be tested for > 0
   * @return value
   * @throws IllegalArgumentException if value is not greater than zero
   */
  public static long checkGreaterThanZero(long value) {
    if (value <= 0) {
      throw new IllegalArgumentException(
          String.format("value (%d) is not greater than zero", value));
    }
    return value;
  }
}
