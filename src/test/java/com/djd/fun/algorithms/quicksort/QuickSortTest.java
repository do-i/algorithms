package com.djd.fun.algorithms.quicksort;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class QuickSortTest {

  @Test
  void sort() {
    int[] data = {4, 5, 2, 1, 3};
    QuickSort.sort(data);
    assertThat(data).containsExactly(1, 2, 3, 4, 5);
  }

  @Test
  void sort_2items() {
    int[] data = {8, 7};
    QuickSort.sort(data);
    assertThat(data).containsExactly(7, 8);
  }

  @Test
  void sort_empty() {
    int[] data = {};
    QuickSort.sort(data);
    assertThat(data).isEmpty();
  }

  @Test
  void partition() {
    int[] data = {5, 6, 1, 0, 8, 2, 4};
    int partitionIdx = QuickSort.partition(data, 0, 6);
    assertThat(partitionIdx).isEqualTo(3);
  }

  @Test
  void swap() {
    int[] data = {5, 6};
    QuickSort.swap(data, 0, 1);
    assertThat(data).containsExactly(6, 5);
  }
}
