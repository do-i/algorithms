package com.djd.fun.algorithms.binpacking;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.google.common.collect.ImmutableList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FirstFitDecreasingTest {

  @BeforeEach
  void setUp() {}

  @Test
  void solve() {
    FirstFitDecreasing firstFitDecreasing =
        new FirstFitDecreasing(
            ImmutableList.of(
                MediaFileItem.from("A", 2),
                MediaFileItem.from("B", 5),
                MediaFileItem.from("C", 4),
                MediaFileItem.from("D", 7),
                MediaFileItem.from("E", 1),
                MediaFileItem.from("F", 3),
                MediaFileItem.from("G", 8)),
            10);
    assertThat(firstFitDecreasing.solve()).isEqualTo(3);
  }

  @Test
  void solve_tooSmallBinCapacity() {
    assertThrows(
        IllegalArgumentException.class,
        () ->
            new FirstFitDecreasing(
                ImmutableList.of(MediaFileItem.from("H", 2), MediaFileItem.from("I", 12)), 10));
  }
}
